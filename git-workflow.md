## Git Workflow ##
By Curvian Vynes  
(Based on **_A successful Git branching model_**
by Vincent Driessen - http://nvie.com/posts/a-successful-git-branching-model/)
___

Two main branches tracking origins:

**master**  
**develop** (or development)

#### DEVELOP ####
Make changes in develop  
- branch off into (create) feature/topic branches for each new feature/topic  
	`git checkout -b some-new-feature`  
- commit often (useful for change log generation and revision)  
    `git commit -m "Add this cool function"`  
    `git commit -m "Drop use of some lib"`  
- merge back into develop with --no-ff option (create new merge commit even if it's a fast-forward commit. useful for grouping all feature branch commits - see workflow model - http://nvie.com/posts/a-successful-git-branching-model/).  
	`git checkout develop`  
	`git merge --no-ff some-new-feature`  
    \**Supply merge commit message, default is good*\*


#### RELEASE ####
Create release-x.y.z branch for releases 
(Only branches to ever be merged into master are release or hotfix branches).  
- when ready for release, branch off from develop to release branch  
	`git checkout -b release-1.2.0 develop`  
- do release specific tweaks in this release branch.  
	\**do some version number bumping in your file(s)*\*  
	`git commit -am "Bumped version to 1.2.0"`
- commit release specific tweaks and cleanups directly to the release branch. branch off for hotfixes, make changes and merge back to release and development branches.  
	\**do some release specific tweaks*\*  
    `git commit -am "Cleanup readme"`  
    \**Realise there's a tiny bug in the release*\*  
	`git checkout -b "a-hotfix"`  
	\**do some bugfixing*\*  
	`git commit -am "encode text correctly"`
	\**do some more bugfixing if needed*\*  
	`git commit -am "change output to more human readable format"`  
	`git checkout release-1.2.0`  
	`git merge --no-ff a-hotfix`  
	\**Supply merge commit message, default is good*\*  
	`git checkout develop`  
	`git merge --no-ff a-hotfix`  
	\**Supply merge commit message, default is good*\*  
	`git branch -d a-hotfix`  
    \**if necessary, checkout release branch again and do some more cleanup/tweaking, committing changes*\*
- merge release branch into master and tag accordingly  
	`git checkout master`  
	`git merge --no-ff release-1.2.0`  
	\**Supply merge commit message, default is good*\*  
	`git tag -a 1.2.0`  
	`git push --follow-tags` (same as `git push && git push --tags`)  
- merge into development branch and delete release branch  
	`git checkout develop`  
	`git merge --no-ff release-1.2.0`  
	\**Supply merge commit message, default is good*\*  
	`git push`  
	`git branch -d release-1.2.0`  


#### MASTER HOTFIXES ####
Hotfixes for severe bugs can be done on master.  
- Create hotfix branch off master 
- Fix some stuff and commit  
- Merge (--no-ff) hotfix into master and development branches  
- Delete hotfix  



*Bada beng!*